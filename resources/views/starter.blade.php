@extends('layouts.app')

{{-- Title Page--}}
@section('title', '')

{{-- Add Library CSS--}}
@section('css')

@endsection

{{-- Main Content--}}
@section('content')
    <div class="section-header">
        <h1>Blank Page</h1>
    </div>
@endsection

{{-- Content for using modal--}}
@section('modal-content')

@endsection

{{-- Add Library JavaScript--}}
@section('js')

@endsection
