@extends('layouts.app')

{{-- Title Page--}}
@section('title', 'Detail WO')

{{-- Add Library CSS--}}
@section('css')
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endsection

{{-- Main Content--}}
@section('content')
    <div class="section-header">
        <h1>Work Order Detail</h1>
    </div>
    <div class="row">
        <div class="col-8">
            <div class="card card-secondary">
                <div class="card-header">
                    <h4>Detail</h4>
                </div>
                <div class="card-body p-0">
                    <table class="table">
                        <tbody>
                        <tr>
                            <th style="width: 20%;">WO ID</th>
                            <td style="width: 2px;">:</td>
                            <td>WO-124124</td>
                        </tr>
                        <tr>
                            <th>Client</th>
                            <td>:</td>
                            <td>PT. Pertamina</td>
                        </tr>
                        <tr>
                            <th>Type Product</th>
                            <td>:</td>
                            <td>Tower Communication</td>
                        </tr>
                        <tr>
                            <th>Total Order</th>
                            <td>:</td>
                            <td>5 Pcs</td>
                        </tr>
                        <tr>
                            <th>Total Progress</th>
                            <td>:</td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" data-width="25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">25%</div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-body">
                    <div class="section-title">Created by</div>
                    <table class="table">
                        <thead class="thead-light">
                        <tr style="text-align: center;">
                            <th scope="col">Person</th>
                            <th scope="col">Date</th>
                            <th scope="col">Due Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Completed?</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr style="text-align: center;">
                            <th scope="row">Deyen</th>
                            <td>11/12/2020</td>
                            <td>11/12/2020</td>
                            <td><div class="badge badge-success">Done</div></td>
                            <td>On Time</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card card-info">
                <div class="card-header">
                    <h4>Progress</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Engineer</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="progressEngineer"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Purchasing</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="progressPurchase"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card card-danger">
                <div class="card-header">
                    <h4>List Of Attachment</h4>
                </div>
                <div class="card-body">
                    <div class="list-group list-group-horizontal-lg text-center" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action list-group-item-secondary active" type="button" id="list-listMarketing" data-toggle="list" data-target="#listMarketing" role="tab">
                            <i class="far fa-copy"></i> Marketing
                        </a>
                        <a class="list-group-item list-group-item-action list-group-item-secondary" type="button" id="list-listEngineer" data-toggle="list" data-target="#listEngineer" role="tab">
                            <i class="fas fa-tools"></i> Engineer
                        </a>
                        <a class="list-group-item list-group-item-action list-group-item-secondary" type="button" id="list-listPurchase" data-toggle="list" data-target="#listPurchase" role="tab">
                            <i class="fas fa-money-bill"></i> Purchasing
                        </a>
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="listMarketing" role="tabpanel" aria-labelledby="list-listMarketing">
                            <hr>
                            <table class="table table-hover text-center">
                                <thead>
                                <tr>
                                    <th style="width: 10%" scope="col">No.</th>
                                    <th scope="col">Specification</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Requirement</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-secondary">
                                            <i class="fas fa-download"></i>
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Task</td>
                                    <td>
                                        <a href="#" class="btn btn-outline-secondary">
                                            <i class="fas fa-download"></i>
                                        </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="listEngineer" role="tabpanel" aria-labelledby="list-listEngineer">
                            <hr>
                            <div class="card-body p-0">
                                <div class="table-responsive">
                                    <table class="table table-striped text-center" id="table-1">
                                        <thead>
                                        <tr>
                                        <tr style="text-align: center;">
                                            <th style="width: 10px;">No.</th>
                                            <th>Category</th>
                                            <th>Part Number</th>
                                            <th>Description</th>
                                            <th>Unit Total</th>
                                            <th>UoM</th>
                                            <th>Specification</th>
                                            <th>Date</th>
                                            <th>Remarks</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Fastener</td>
                                            <td>BNM825</td>
                                            <td>Bolt & Nut M8*25</td>
                                            <td>10</td>
                                            <td>Pairs</td>
                                            <td>8.8</td>
                                            <td>16/01/2020</td>
                                            <td>Deyen</td>
                                        </tr><tr>
                                            <td>3</td>
                                            <td>Fastener</td>
                                            <td>BNM825</td>
                                            <td>Bolt & Nut M8*25</td>
                                            <td>10</td>
                                            <td>Pairs</td>
                                            <td>8.8</td>
                                            <td>16/01/2020</td>
                                            <td>Deyen</td>
                                        </tr><tr>
                                            <td>2</td>
                                            <td>Fastener</td>
                                            <td>BNM825</td>
                                            <td>Bolt & Nut M8*25</td>
                                            <td>10</td>
                                            <td>Pairs</td>
                                            <td>8.8</td>
                                            <td>16/01/2020</td>
                                            <td>Haikal</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="listPurchase" role="tabpanel" aria-labelledby="list-listPurchase">
                            <hr>
                            <p>
                                404 - Not Available Now
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Add Library JavaScript--}}
@section('js')
    <script src="{{ asset('stisla/node_modules/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $("#table-1").dataTable({
            "paging": false,
            "info": false,
            "columnDefs": [
                { "sortable": false, "targets": [2,3,4,5,6,] }
            ]
        });
    </script>
    <script>
        var ctx = document.getElementById("progressEngineer").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        {{ '100-24' }},
                        {{ '24' }},
                    ],
                    backgroundColor: [
                        '#6777ef',
                        '#D3D3D3',
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Done',
                    'In Progress',
                ],
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
            }
        });

        var ctx = document.getElementById("progressPurchase").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        30,
                        70,
                    ],
                    backgroundColor: [
                        '#6777ef',
                        '#D3D3D3',
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Done',
                    'In Progress',
                ],
            },
            options: {
                responsive: true,
                legend: {
                    position: 'bottom',
                },
            }
        });
    </script>
@endsection
