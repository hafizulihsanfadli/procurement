@extends('layouts.app')

{{-- Title Page--}}
@section('title', 'Add Bill Of Materials')

{{-- Add Library CSS--}}
@section('css')
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/select2/dist/css/select2.min.css') }}">
@endsection

{{-- Main Content--}}
@section('content')
    <div class="section-header">
        <h1>Add Bill Of Materials</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <form action="#" method="post">
                    <div class="card-body">
                        <div class="list-group list-group-horizontal-lg text-center" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action list-group-item-secondary active" type="button" id="list-listExistingMaterial" data-toggle="list" data-target="#listExistingMaterial" role="tab">
                                <i class="fas fa-database"></i> Existing Material
                            </a>
                            <a class="list-group-item list-group-item-action list-group-item-secondary" type="button" id="list-listNewMaterial" data-toggle="list" data-target="#listNewMaterial" role="tab">
                                <i class="fas fa-star-of-life"></i> Add New Material
                            </a>
                        </div>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="listExistingMaterial" role="tabpanel" aria-labelledby="list-listExistingMaterial">
                                <div class="form-group row mb-3">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Material</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2">
                                            <option>(FASTENER) P448 - Plate 4mm x 4' x 8'</option>
                                            <option>(FASTENER) BNM825 - Bolt & Nut M8 x 25</option>
                                            <option>(STEEL & STRUCTURE) ST1501505 - Square Tube 150 x 150 x 5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="listNewMaterial" role="tabpanel" aria-labelledby="list-listNewMaterial">
                                <div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Category</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Category ...">
                                    </div>
                                </div><div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Part Number</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Part Number ...">
                                    </div>
                                </div><div class="form-group row">
                                    <label for="inputEmail3" class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="inputEmail3" placeholder="Description ...">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Unit Total</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Work Order ...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">UoM</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Client ...">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Specification</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Type Product ...">
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

{{-- Content for using modal--}}
@section('modal-content')

@endsection

{{-- Add Library JavaScript--}}
@section('js')
    <script src="{{ asset('stisla/node_modules/select2/dist/js/select2.full.min.js') }}"></script>
@endsection
