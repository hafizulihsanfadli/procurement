@extends('layouts.app')

{{-- Title Page--}}
@section('title', 'Dashboard')

{{-- Add Library CSS--}}
@section('css')

@endsection

{{-- Main Content--}}
@section('content')
{{--    <div class="section-header">--}}
{{--        <h1>Dashboard</h1>--}}
{{--    </div>--}}

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h4>List of Work Order</h4>
                    <div class="card-header-action">
                        <a href="{{ route('all') }}" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive table-invoice">
                        <table class="table table-striped">
                            <tbody>
                            <tr style="text-align: center;">
                                <th style="width: 10px;">No.</th>
                                <th>Work Order No.</th>
                                <th>Type Product</th>
                                <th>Client</th>
                                <th>Due Date</th>
                                <th>Progress</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td><a href="#">WO-87239</a></td>
                                <td class="font-weight-600">Tower</td>
                                <td class="font-weight-600">PT. Angkasa Pura 2</td>
                                <td style="text-align: center;">July 19, 2018</td>
                                <td style="text-align: center;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" data-width="65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">65%</div>
                                    </div>
                                </td>
                                <td style="text-align: center;"><div class="badge badge-warning">In Progress</div></td>
                                <td style="text-align: center;">
                                    <a href="{{ route('detail') }}" class="btn btn-primary">Detail</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">2</td>
                                <td><a href="#">WO-48574</a></td>
                                <td class="font-weight-600">Tower</td>
                                <td class="font-weight-600">PT. Pertamina</td>
                                <td style="text-align: center;">July 21, 2018</td>
                                <td style="text-align: center;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" data-width="25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%;">25%</div>
                                    </div>
                                </td>
                                <td style="text-align: center;"><div class="badge badge-warning">In Progress</div></td>
                                <td style="text-align: center;">
                                    <a href="{{ route('detail') }}" class="btn btn-primary">Detail</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Add Library JavaScript--}}
@section('js')

@endsection
