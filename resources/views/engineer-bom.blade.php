@extends('layouts.app')

{{-- Title Page--}}
@section('title', 'Bill Of Material')

{{-- Add Library CSS--}}
@section('css')
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endsection

{{-- Main Content--}}
@section('content')
    <div class="section-header">
        <h1>Bill Of Material</h1>
    </div>
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card mb-1">
                <div class="card-header ">
                    <div class="col-8 ml-n4">
                        <div class="table-responsive-sm mt-n2 mb-n4">
                            <table class="table table-sm d-sm-table table-striped text-center">
                                <thead>
                                <tr>
                                    <td>Work Order</td>
                                    <td>Client</td>
                                    <td>Type Product</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1234567890123456</td>
                                    <td>PT. VALE Indonesia Jaya Sejahtera Tbk</td>
                                    <td>Tower Lighting Mast (TEMPORARY)</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <h4> </h4>
                    <div class="card-header-action">
                        <button class="btn btn-icon btn-outline-secondary" data-toggle="tooltip" data-placement="bottom" title="Bill of Materials" onclick="window.location.href='{{ route('bom-add') }}'"><i class="fas fa-plus-circle"></i> Add BoM</button>
                        <button class="btn btn-icon btn-outline-dark" data-toggle="modal" data-target="#uploadModal"><i class="fas fa-upload"></i> Drawing</button>
                        <button class="btn btn-icon btn-outline-danger" data-toggle="tooltip" data-placement="bottom" title="Set Percentage"><i class="fas fa-chart-pie"></i> 100%</button>
                        <button data-collapse="#mycard-collapse2" class="btn btn-icon btn-info ml-2" data-toggle="tooltip" data-placement="bottom" title="Show/Hide"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
                <div class="collapse" id="mycard-collapse2">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped text-center" id="table-1">
                                <thead>
                                <tr>
                                <tr style="text-align: center;">
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Part Number</th>
                                    <th>Description</th>
                                    <th>Unit Total</th>
                                    <th>UoM</th>
                                    <th>Specification</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen (e)</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        Add Bill Of Material
                    </div>
                </div>
            </div>
            <div class="card mb-1">
                <div class="card-header ">
                    <div class="col-9 ml-n4">
                        <div class="table-responsive-sm mt-n2 mb-n4">
                            <table class="table table-sm d-sm-table table-striped text-center">
                                <thead>
                                <tr>
                                    <td>Work Order</td>
                                    <td>Client</td>
                                    <td>Type Product</td>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>12312412412</td>
                                    <td>PT. Pertamina</td>
                                    <td>Tower</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <h4> </h4>
                    <div class="card-header-action">
                        <button class="btn btn-icon btn-outline-secondary" href="#"><i class="fas fa-plus-circle"></i> Add Bill of Materials</button>
                        <a data-collapse="#mycard-collapse" class="btn btn-icon btn-info" href="#"><i class="fas fa-plus"></i></a>
                    </div>
                </div>
                <div class="collapse" id="mycard-collapse">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped text-center" id="table-1">
                                <thead>
                                <tr>
                                <tr style="text-align: center;">
                                    <th>Date</th>
                                    <th>Category</th>
                                    <th>Part Number</th>
                                    <th>Description</th>
                                    <th>Unit Total</th>
                                    <th>UoM</th>
                                    <th>Specification</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>16/01/2020</td>
                                    <td>Fastener</td>
                                    <td>BNM825</td>
                                    <td>Bolt & Nut M8*25</td>
                                    <td>10</td>
                                    <td>Pairs</td>
                                    <td>8.8</td>
                                    <td>Deyen</td>
                                    <td>
                                        <button class="btn btn-sm btn-outline-secondary">Edit</button>
                                        <button class="btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        Add Bill Of Material
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Content for using modal--}}
@section('modal-content')
    <div class="modal fade" tabindex="-1" role="dialog" id="uploadModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload File Drawing</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="list-group list-group-horizontal-lg text-center" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action list-group-item-secondary active" type="button" id="list-listSingleUpload" data-toggle="list" data-target="#listSingleUpload" role="tab">
                            <i class="fas fa-file-upload"></i> Single File
                        </a>
                        <a class="list-group-item list-group-item-action list-group-item-secondary" type="button" id="list-listMultipleUpload" data-toggle="list" data-target="#listMultipleUpload" role="tab">
                            <i class="fas fa-upload"></i> Multiple Files
                        </a>
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="listSingleUpload" role="tabpanel" aria-labelledby="list-listSingleUpload">
                            <hr>
                            <div class="section-title" id="upload-title">Upload a file</div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile"></label>
                            </div>
                        </div>
                        <div class="tab-pane fade text-center" id="listMultipleUpload" role="tabpanel" aria-labelledby="list-listMultipleUpload">
                            <hr>
                            <p>
                                404 - Not Available
                            </p>
                            <p class="text-danger">Sorry, this feature is not available right now!</p>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Add Library JavaScript--}}
@section('js')
    <script src="{{ asset('stisla/node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
    <script>
        $("#table-1").dataTable({
            "paging": false,
            "info": false,
            "columnDefs": [
                { "sortable": false, "targets": [2,3,4,5,6,8] }
            ]
        });

        function readURL(input) {
            var file = input.files[0];
            var filename = file.name;
            console.log(filename)
            $('#upload-title').text('Filename to upload : ' + filename);
        }
        $("#customFile").change(function () {
            readURL(this);
            console.log('1');
        })
    </script>
@endsection
