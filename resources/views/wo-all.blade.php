@extends('layouts.app')

{{-- Title Page--}}
@section('title', 'All WO')

{{-- Add Library CSS--}}
@section('css')
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('stisla/node_modules/datatables.net-select-bs4/css/select.bootstrap4.min.css') }}">
@endsection

{{-- Main Content--}}
@section('content')
    <div class="section-header">
        <h1>List All Work Order</h1>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body p-2">
                    <div class="table-responsive">
                        <table class="table table-striped" id="table-1">
                            <thead>
                            <tr>
                            <tr style="text-align: center;">
                                <th style="width: 10px;">No.</th>
                                <th>Work Order No.</th>
                                <th>Type Product</th>
                                <th>Client</th>
                                <th>Due Date</th>
                                <th>Progress</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td style="text-align: center;">1</td>
                                <td><a href="#">WO-87239</a></td>
                                <td>Tower</td>
                                <td class="font-weight-600">PT. Angkasa Pura 2</td>
                                <td style="text-align: center;">July 19, 2018</td>
                                <td style="text-align: center;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" data-width="65%" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">65%</div>
                                    </div>
                                </td>
                                <td style="text-align: center;"><div class="badge badge-warning">In Progress</div></td>
                                <td style="text-align: center;">
                                    <div class="btn-group">
                                        <a href="{{ route('detail') }}" class="btn btn-outline-info" data-toggle="tooltip" data-placement="left" title="Details"><i class="far fa-eye"></i></a>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="far fa-edit"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="#uploadModal" data-toggle="modal">Upload Specification</a>
                                                <a class="dropdown-item" href="{{ route('wo-add') }}">Edit Work Order</a>
                                            </div>
                                        </div>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-outline-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-archive"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" id="swal-completed" href="#">Project Completed</a>
                                                <a class="dropdown-item" href="#">Project Canceled</a>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">2</td>
                                <td><a href="#">WO-48574</a></td>
                                <td>Tower</td>
                                <td class="font-weight-600">PT. Pertamina</td>
                                <td style="text-align: center;">July 21, 2018</td>
                                <td style="text-align: center;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" data-width="100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">100%</div>
                                    </div>
                                </td>
                                <td style="text-align: center;"><div class="badge badge-success">Completed</div></td>
                                <td style="text-align: center;">
                                    <div class="btn-group">
                                        <a href="{{ route('detail') }}" class="btn btn-outline-info"><i class="far fa-eye"></i></a>
                                        <a href="{{ route('detail') }}" class="btn btn-outline-secondary"><i class="far fa-edit"></i></a>
                                        <button href="{{ route('detail') }}" class="btn btn-outline-success" disabled="disabled"><i class="fas fa-check"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;">3</td>
                                <td><a href="#">WO-32574</a></td>
                                <td>Tower</td>
                                <td class="font-weight-600">PT. Wika</td>
                                <td style="text-align: center;">July 21, 2018</td>
                                <td style="text-align: center;">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" data-width="100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">100%</div>
                                    </div>
                                </td>
                                <td style="text-align: center;"><div class="badge badge-danger">Canceled</div></td>
                                <td style="text-align: center;">
                                    <div class="btn-group">
                                        <a href="{{ route('detail') }}" class="btn btn-outline-info"><i class="far fa-eye"></i></a>
                                        <a href="{{ route('detail') }}" class="btn btn-outline-secondary"><i class="far fa-edit"></i></a>
                                        <button href="{{ route('detail') }}" class="btn btn-outline-danger" disabled="disabled"><i class="fas fa-times"></i></button>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Content for using modal--}}
@section('modal-content')
    <div class="modal fade" tabindex="-1" role="dialog" id="uploadModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload File Specification</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="list-group list-group-horizontal-lg text-center" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action list-group-item-secondary active" type="button" id="list-listSingleUpload" data-toggle="list" data-target="#listSingleUpload" role="tab">
                            <i class="fas fa-file-upload"></i> Single File
                        </a>
                        <a class="list-group-item list-group-item-action list-group-item-secondary" type="button" id="list-listMultipleUpload" data-toggle="list" data-target="#listMultipleUpload" role="tab">
                            <i class="fas fa-upload"></i> Multiple Files
                        </a>
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="listSingleUpload" role="tabpanel" aria-labelledby="list-listSingleUpload">
                            <hr>
                            <div class="section-title" id="upload-title">Upload a file</div>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="customFile">
                                <label class="custom-file-label" for="customFile"></label>
                            </div>
                        </div>
                        <div class="tab-pane fade text-center" id="listMultipleUpload" role="tabpanel" aria-labelledby="list-listMultipleUpload">
                            <hr>
                            <p>
                                404 - Not Available
                            </p>
                            <p class="text-danger">Sorry, this feature is not available right now!</p>
                        </div>
                    </div>
                    <div class="text-right">
                        <button type="button" class="btn btn-primary">Upload</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

{{-- Add Library JavaScript--}}
@section('js')
    <script src="{{ asset('stisla/node_modules/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('stisla/node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>

    <script type="text/javascript">
        $("#swal-completed").click(function() {
            swal({
                title: 'Are you sure?',
                text: 'Once archived, you will not be able to recover this work order!',
                icon: 'warning',
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        swal('Success! Your work order has been archived!', {
                            icon: 'success',
                            buttons: false,
                            timer: 1200,
                        });
                    } else {
                        swal('Your work order is safe!', {
                            buttons: false,
                            timer: 800,
                        });
                    }
                });
        });

        $("#table-1").dataTable({
            "paging": false,
            "info": false,
            "columnDefs": [
                { "sortable": false, "targets": [2,3,7] }
            ]
        });

        function readURL(input) {
            var file = input.files[0];
            var filename = file.name;
                console.log(filename)
            $('#upload-title').text('Filename to upload : ' + filename);
        }
        $("#customFile").change(function () {
            readURL(this);
            console.log('1');
        })
    </script>
@endsection
