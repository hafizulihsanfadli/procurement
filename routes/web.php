<?php

Auth::routes(['register' => false]);

Route::get('/', function () {
    return view('starter');
});
Route::get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
Route::get('/workorder/list', function () {
    return view('wo-all');
})->name('all');
Route::get('/workorder/detail', function () {
    return view('wo-detail');
})->name('detail');
Route::get('/workorder/add', function () {
    return view('wo-new');
})->name('wo-add');
Route::get('/bom', function () {
    return view('engineer-bom');
})->name('bom');
Route::get('/bom/add', function () {
    return view('engineer-bom-add');
})->name('bom-add');
Route::get('/welcome', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');
